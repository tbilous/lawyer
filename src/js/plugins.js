// Avoid `console` errors in browsers that lack a console.
(function () {
    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
var b = document.documentElement;
b.setAttribute('data-useragent', navigator.userAgent);
b.setAttribute('data-platform', navigator.platform);

// IE 10 == Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)


$(document).ready(function () {

//Form
    jQuery(function ($) {
       $('input[type=tel]').mask("+(38) (099) 999-99-99");
    });

    $('#feedbackModal').on('shown.bs.modal', function () {
        var targetInput = $(this).find('input:nth-of-type(2)');
        console.log(targetInput);
        $(targetInput).focus()
    });


    /*jQuery time*/
    activeAccordian = function () {
        if($(window).width() >= 764) {
            $('#accordian').find('.active ul').fadeIn()
        }
        else {
            $('#accordian').find('.active').removeClass('active')
        }
    };

    activeAccordian();

    $("#accordian .accordion-title").click(function () {
        var target = $(this).closest('li');

        if (!target.hasClass('active')) {
            $('#accordian').each(function () {
                var active = $('#accordian ').find('.active');
                active.find('ul').fadeOut(100);
                active.removeClass('active');
            });
            target.closest('li').addClass('active');
            $(this).next().fadeIn(600);

        }
        else {
            target.closest('li').removeClass('active');
            $(this).next().fadeOut(100);
        }

    });
    $('.staff-content').hover(function () {
        $(this).find('.staff-link').fadeIn().addClass('active')
    }, function () {
            $(this).find('.staff-link').fadeOut().removeClass('active')
        }
    );
});
